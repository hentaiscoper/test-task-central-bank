import React from 'react';
import './App.css';
import Tree from "./components/tree/tree";

function App() {
  return (
    <div className="app">
        <Tree/>
    </div>
  );
}

export default App;
