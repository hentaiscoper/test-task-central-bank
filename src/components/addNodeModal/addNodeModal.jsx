import React, {Component} from 'react';
import { connect } from "react-redux";
import './addNodeModal.scss';
import {closeModal, setAddNodeError} from "../../actions/actions";
import ReactSVG from "react-svg";
import icons from "../../constants/icons";
import { last } from "lodash";

const initialState = {
    name: '',
};

class AddNodeModal extends Component {
    state = initialState;

    handleChange = e => {
        this.setState({
            name: e.target.value
        })
    };

    closeModal = () => {
        const { dispatch } = this.props;
        dispatch(closeModal());
        this.setState(initialState);
    };

    handleFormSubmit = e => {
        e.preventDefault();
        const { modal: { modalNode }, addNode, dispatch } = this.props;
        const { name } = this.state;
        if(name.length > 0)
            addNode(modalNode, modalNode + '/' + name.toLowerCase().split(' ').map(word => (
                word[0].toUpperCase() + word.slice(1,word.length))).join(''));
        else
            dispatch(setAddNodeError('Node name field is blank'));
        this.setState(initialState);
    };
    render() {
        const { modal: { isModalOpen, modalNode, error } } = this.props;
        return (
            <>
                {isModalOpen &&
                    <>
                        <div className="modal__wrapper"/>
                        <div className="modal">
                            <div className="modal__header">
                                <div className="modal__title">
                                    Add new child node to "{last(modalNode.split("/"))}":
                                </div>
                                <button
                                    className="modal__button modal__button--close"
                                    onClick={() => this.closeModal()}
                                >
                                    <ReactSVG src={icons.delete_icon}/>
                                </button>
                            </div>
                            <form className="modal__content" onSubmit={this.handleFormSubmit}>
                                <input
                                    className="modal__input"
                                    onChange={this.handleChange}
                                    value={this.state.name}
                                    type="text"
                                />
                                <div className="modal__form-helper-text">
                                    {error}
                                </div>
                                <button
                                    className="modal__button modal__button--submit"
                                    type="submit"
                                >
                                    Submit
                                </button>
                            </form>
                            <div>

                            </div>
                        </div>
                    </>
                }
            </>
        );
    }
}

const mapStateToProps = state => ({
    isModalOpen: state.isModalOpen,
    modal: state.modal,
});

export default connect(mapStateToProps) (AddNodeModal);