import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { last } from 'lodash';

import ReactSVG from "react-svg";
import icons from "../../constants/icons";

import './treeNode.scss';


class TreeNode extends Component {
    render() {
        const { path, getNode, getChildNodes, changeNodeStatus, deleteNode, addNodeRequest } = this.props;
        const node = getNode(path);
        if(typeof node !== "undefined")
        return (
            <div className="tree-node">
                <div className="tree-node__element">
                    { node.children.length > 0 &&
                        <button
                            className="tree-node__button tree-node__button--expand"
                            onClick={() => changeNodeStatus(path)}
                        >
                            <ReactSVG
                                className={`tree-node__svg tree-node__arrow${node.isOpen ? '--down' : '--right' }`}
                                src={icons.arrow}
                            />
                        </button>
                    }
                    <div className="tree-node__name">
                        {last(node.path.split('/'))}
                    </div>
                    <div className="tree-node__control">
                        { !node.isRoot &&
                            <button
                                className="tree-node__button"
                                onClick={() => deleteNode(node.path)}
                            >
                                <ReactSVG
                                    className="tree-node__svg"
                                    src={icons.delete_icon}
                                />
                            </button>
                        }
                        <button
                            className="tree-node__button"
                            onClick={() => addNodeRequest(node.path)}
                        >
                            <ReactSVG src={icons.add}/>
                        </button>
                    </div>
                </div>
                <div className="tree-node__children">
                    {node.isOpen && getChildNodes(path).map((node, index) => (
                        <TreeNode
                            key={index}
                            path={node.path}
                            getNode={getNode}
                            getChildNodes={getChildNodes}
                            changeNodeStatus={changeNodeStatus}
                            deleteNode={deleteNode}
                            addNodeRequest={addNodeRequest}
                        />
                    ))}
                </div>
            </div>
        );
    }
}

TreeNode.propTypes = {
    path: PropTypes.string.isRequired,
    getNode: PropTypes.func.isRequired,
    getChildNodes: PropTypes.func.isRequired,
    changeNodeStatus: PropTypes.func.isRequired,
    deleteNode: PropTypes.func.isRequired,
    addNodeRequest: PropTypes.func.isRequired,
};

export default TreeNode;