import React, { Component } from 'react';
import TreeNode from "../treeNode/treeNode";
import { connect } from "react-redux";
import { values, xor } from 'lodash';
import './tree.scss';
import {
    addNode,
    addNodeRequest,
    changeDisplayMode,
    changeNodeStatus,
    deleteNode,
    setAddNodeError
} from "../../actions/actions";
import AddNodeModal from "../addNodeModal/addNodeModal";
import JSONPretty from "react-json-pretty";

class Tree extends Component {
    getNode = currentNode => {
        const { nodes } = this.props;
        return values(nodes).find(node => node.path === currentNode)
    };

    getRootNode = currentNode =>
        currentNode.split('/').slice(0,currentNode.split('/').length - 1).join('/');

    changeNodeStatus = currentNode => {
        const { dispatch } = this.props;
        dispatch(changeNodeStatus(currentNode))
    };

    getChildNodes = currentNode => {
        const { nodes } = this.props;
        const { children } = nodes[currentNode];
        if (typeof children === 'undefined' || children.length === 0) {
            return [];
        }
        else {
            return children.map(node => nodes[node]);
        }
    };

    deepDelete = currentNode =>
        [].concat(currentNode, ...this.props.nodes[currentNode].children.map(node => this.deepDelete(node)));

    deleteNode = currentNode => {
        const { dispatch, nodes } = this.props;
        const rootNode = this.getRootNode(currentNode);
        const nodesToDelete = this.deepDelete(currentNode);
        let newNodes = {};

        values(nodes).forEach(node => {
            if(nodesToDelete.indexOf(node.path) === -1)
                newNodes = { ...newNodes, [node.path]: node }
        });

        dispatch(deleteNode({
            nodes: {
                ...newNodes,
                [rootNode]: {
                    ...nodes[rootNode],
                    children: [...xor(nodes[rootNode].children, [currentNode])]
                }
            },
        }))
    };

    addNodeRequest = (currentNode) => {
        const { dispatch } = this.props;
        dispatch(addNodeRequest(currentNode));
    };

    addNode = (currentNode, newNode) => {
        const { dispatch, nodes } = this.props;
        if(nodes.hasOwnProperty(newNode))
            dispatch(setAddNodeError('This name already exist'));
        else
            dispatch(addNode({
                [currentNode]: {
                    ...nodes[currentNode],
                    children: [ ...nodes[currentNode].children, newNode]
                },
                [newNode]: {
                    path: newNode,
                    isOpen: false,
                    children: [],
                }
            }));
    };

    changeDisplayMode = () => {
        const { dispatch } = this.props;
        dispatch(changeDisplayMode())
    };

    render() {
        const { addNode, addNodeRequest, getNode, getChildNodes, changeNodeStatus, deleteNode, changeDisplayMode } = this;
        const { treeMode, nodes } = this.props;
        return (
            <>
                <AddNodeModal addNode={addNode}/>
                <div className="app__title">
                    ReactJS tree view
                </div>
                <button className="tree-root__button" onClick={() => changeDisplayMode()}>Change Display Mode</button>
                    <div className="tree-root">
                        { treeMode ?
                            <TreeNode
                                path={'/root'}
                                getNode={getNode}
                                getChildNodes={getChildNodes}
                                changeNodeStatus={changeNodeStatus}
                                addNodeRequest={addNodeRequest}
                                deleteNode={deleteNode}
                            /> :
                            <JSONPretty data={JSON.stringify(nodes)}/>
                        }
                    </div>
            </>
        );
    }
}

const mapStateToProps = state => ({
    nodes: state.nodes,
    treeMode: state.treeMode,
});

export default connect (mapStateToProps) (Tree);