import arrow from '../assets/icons/arrow.svg';
import add from '../assets/icons/add.svg';
import delete_icon from '../assets/icons/delete.svg';

const icons = {
    arrow,
    add,
    delete_icon
};

export default icons;