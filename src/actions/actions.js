export const DELETE_NODE = "GET_CHILD_NODES";
export const ADD_NODE_REQUEST = "ADD_NODE_REQUEST";
export const ADD_NODE = "ADD_NODE";
export const CLOSE_MODAL = "CLOSE_MODAL";
export const SET_ADD_NODE_ERROR = "SET_ADD_NODE_ERROR";
export const CHANGE_NODE_STATUS = "CHANGE_NODE_STATUS";
export const CHANGE_DISPLAY_MODE = "CHANGE_DISPLAY_MODE";

export const deleteNode = payload => ({
    type: DELETE_NODE,
    payload,
});

export const addNodeRequest = payload => ({
    type: ADD_NODE_REQUEST,
    payload,
});

export const addNode = payload => ({
    type: ADD_NODE,
    payload,
});

export const closeModal = () => ({
    type: CLOSE_MODAL,
});

export const setAddNodeError = payload => ({
    type: SET_ADD_NODE_ERROR,
    payload,
});

export const changeNodeStatus = payload => ({
    type: CHANGE_NODE_STATUS,
    payload,
});

export const changeDisplayMode = payload => ({
    type: CHANGE_DISPLAY_MODE,
    payload,
});