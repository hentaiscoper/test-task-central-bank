import {
    DELETE_NODE,
    ADD_NODE,
    CHANGE_NODE_STATUS,
    ADD_NODE_REQUEST,
    CLOSE_MODAL,
    SET_ADD_NODE_ERROR, CHANGE_DISPLAY_MODE
} from "../actions/actions";

const nodes = {
    '/root': {
        path: '/root',
        isRoot: true,
        isOpen: true,
        children: ['/root/music', '/root/images', '/root/webStormProjects'],
    },
    '/root/music': {
        path: '/root/music',
        isOpen: false,
        children: ['/root/music/boulevardDepo',],
    },
    '/root/images': {
        path: '/root/images',
        isOpen: false,
        children: [],
    },
    '/root/webStormProjects': {
        path: '/root/webStormProjects',
        isOpen: false,
        children: [],
    },
    '/root/music/boulevardDepo': {
        path: '/root/music/boulevardDepo',
        isOpen: false,
        children: ['/root/music/boulevardDepo/otricala'],
    },
    '/root/music/boulevardDepo/otricala': {
        path: '/root/music/boulevardDepo/otricala',
        isOpen: true,
        children: [],
    },
};

const modal = {
    isModalOpen: false,
    modalNode: '',
    error: '',
};

const rootReducer = (state = { nodes, modal, treeMode: true }, action) => {
    switch (action.type) {
        case DELETE_NODE:
            return { ...state, nodes: action.payload.nodes };
        case ADD_NODE_REQUEST:
            return { ...state, modal: { ...modal, isModalOpen: true, modalNode: action.payload } };
        case ADD_NODE:
            return { ...state,
                nodes: {
                    ...state.nodes,
                    ...action.payload,
                },
                modal
            };
        case CLOSE_MODAL:
            return { ...state, modal };
        case SET_ADD_NODE_ERROR:
            return {...state, modal: { ...state.modal, error: action.payload}  };
        case CHANGE_NODE_STATUS:
            return {
                ...state,
                nodes: {
                    ...state.nodes,
                    [action.payload]: {
                        ...state.nodes[action.payload],
                        isOpen: !state.nodes[action.payload].isOpen,
                    }
                }
            };
        case CHANGE_DISPLAY_MODE:
            return {
                ...state,
                treeMode: !state.treeMode,
            };
        default:
            return { ...state };
    }
};

export default rootReducer;